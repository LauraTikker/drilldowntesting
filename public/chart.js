// Define nulti-level data
var data = [
  {
    from: "A",
    to: "D",
    value: 10,
    sub: [
      { from: "A1", to: "D1", value: 2 },
      {
        from: "A2",
        to: "D2",
        value: 5,
        sub: [
          { from: "A1-1", to: "D1-1", value: 1 },
          { from: "A1-1", to: "D1-2", value: 1 },
          { from: "A1-2", to: "D1-2", value: 2 },
          { from: "A1-2", to: "D1-3", value: 1 }
        ]
      },
      { from: "A3", to: "D2", value: 3 }
    ]
  },
  {
    from: "B",
    to: "D",
    value: 8,
    sub: [
      { from: "B1", to: "D1", value: 2 },
      { from: "B2", to: "D2", value: 2 },
      { from: "B3", to: "D2", value: 3 },
      { from: "B4", to: "D2", value: 1 }
    ]
  },
  {
    from: "B",
    to: "E",
    value: 4,
    sub: [
      { from: "B1", to: "E1", value: 2 },
      { from: "B2", to: "E2", value: 1 },
      { from: "B2", to: "E3", value: 1 }
    ]
  },
  {
    from: "C",
    to: "E",
    value: 3,
    sub: [
      { from: "C1", to: "E1", value: 2 },
      { from: "C1", to: "E2", value: 1 }
    ]
  }
];

am4core.useTheme(am4themes_animated);

var chart = am4core.create("chartdiv", am4charts.SankeyDiagram);
chart.data = data;
chart.dataFields.fromName = "from";
chart.dataFields.toName = "to";
chart.dataFields.value = "value";

// for right-most label to fit
chart.paddingRight = 40;
chart.paddingBottom = 25;

// Add events on links
chart.links.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
chart.links.template.events.on("hit", function(ev) {
  chart.colors.reset();
  var nav = chart.children.getIndex(0);
  var linkData = ev.target.dataItem.dataContext;
  if (linkData.sub) {
    chart.data = linkData.sub;
    nav.data.push({
      name: ev.target.populateString("{fromName}→{toName}"),
      step: ev.target.dataItem.dataContext
    });
    nav.invalidateData();
  }
});

// Add navigation bar
var nav = chart.createChild(am4charts.NavigationBar);
nav.data = [{ name: "Home" }];
nav.toBack();

nav.links.template.events.on("hit", function(ev) {
  var target = ev.target.dataItem.dataContext;
  var nav = ev.target.parent;
  chart.colors.reset();
  if (target.step) {
    chart.data = target.step.sub;
    nav.data.splice(nav.data.indexOf(target) + 1);
    nav.invalidateData();
  } else {
    chart.data = data;
    nav.data = [{ name: "Home" }];
  }
});
